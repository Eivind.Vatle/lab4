package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] cellStates;


    public CellGrid(int rows, int columns, CellState initialState) {
		this.cols=columns;
        this.rows=rows;

        cellStates=new CellState[rows][columns];


        for(int j=0;j<rows;j++){
            for(int i=0;i<columns;i++){
                cellStates[j][i]=initialState;

            }

        }


	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if(row<numRows()&&row>=0&&column<numColumns()&&column>=0){
            cellStates[row][column]=element;

        }

        else {
            throw new IndexOutOfBoundsException();


        }
        
    }

    @Override
    public CellState get(int row, int column) {
        if(row<numRows()&&row>=0&&column<numColumns()&&column>=0){
            return cellStates[row][column];


        }
        else {
            throw new IndexOutOfBoundsException();


        }


    }

    @Override
    public IGrid copy() {
        IGrid gridCopy=new CellGrid(this.rows,this.cols,null);

        for(int j=0;j<rows;j++){
            for(int i=0;i<cols;i++){
            gridCopy.set(j,i,get(j,i));


            }

        }

        return gridCopy;
    }
    
}