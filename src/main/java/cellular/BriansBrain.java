package cellular;

public class BriansBrain extends GameOfLife{

    /**
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     *
     * @param rows
     * @param columns
     */
    public BriansBrain(int rows, int columns) {
        super(rows, columns);
    }
    @Override
    public CellState getNextCell(int row, int col){


        CellState state = getCellState(row,col);
        int livingNeighbor=super.countNeighbors(row,col,CellState.ALIVE);


        if(state.equals(CellState.ALIVE)){
            return CellState.DEAD;
        }


        else if(state.equals(CellState.DEAD)&& livingNeighbor==2){
                return CellState.ALIVE;

            }
        else if(state.equals(CellState.ALIVE)&& livingNeighbor==2){
            return CellState.ALIVE;
        }
        else{
            return CellState.DEAD;
        }

    }












}
